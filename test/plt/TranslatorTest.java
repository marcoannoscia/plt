package plt;

import static org.junit.Assert.*;

import org.junit.Test;

import plt.exception.PigLatinException;

public class TranslatorTest {

	@Test
	public void testInputPhrase() throws Exception{
		String inputPhrase = "hello world" ;
		Translator translator = new Translator(inputPhrase) ;
		assertEquals("hello world", translator.getPhrase()) ;
	}
	
	@Test
	public void testTranslationEmptyPhrase() throws Exception{
		String inputPhrase =  "";
		Translator translator = new Translator(inputPhrase) ;
		assertEquals(Translator.NIL, translator.translate()) ;
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithY() throws Exception{
		String inputPhrase =  "any";
		Translator translator = new Translator(inputPhrase) ;
		assertEquals("anynay", translator.translate()) ;
	}
	
	@Test
	public void testTranslationPhraseStartingWithUEndingWithY() throws Exception{
		String inputPhrase =  "utility";
		Translator translator = new Translator(inputPhrase) ;
		assertEquals("utilitynay", translator.translate()) ;
	}

	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithVowel() throws Exception{
		String inputPhrase =  "apple";
		Translator translator = new Translator(inputPhrase) ;
		assertEquals("appleyay", translator.translate()) ;
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithConsonant()throws Exception {
		String inputPhrase =  "ask";
		Translator translator = new Translator(inputPhrase) ;
		assertEquals("askay", translator.translate()) ;
	}
	
	
	@Test
	public void testTranslationPhraseStartingWithSingleConsonant() throws Exception{
		String inputPhrase = "hello" ;
		Translator translator = new Translator(inputPhrase) ;
		assertEquals("ellohay", translator.translate()) ;
	}
	
	
	@Test
	public void testTranslationPhraseStartingWithMoreConsonant() throws Exception {
		String inputPhrase = "known" ;
		Translator translator = new Translator(inputPhrase) ;
		assertEquals("ownknay", translator.translate()) ;
	}
	
	
	@Test
	public void testTranslationPhraseContainingMoreWordsSeparatedBySpaces() throws Exception {
		String inputPhrase = "hello world" ;
		Translator translator = new Translator(inputPhrase) ;
		assertEquals("ellohay orldway", translator.translate()) ;
	}
	
	@Test
	public void testTranslationPhraseContainingMoreWordsSeparatedByDash() throws Exception {
		String inputPhrase = "well-being" ;
		Translator translator = new Translator(inputPhrase) ;
		assertEquals("ellway-eingbay", translator.translate()) ;
	}
	
	@Test
	public void testTranslationPhraseContainingMoreWordsSeparatedBySpacesAndDash() throws Exception {
		String inputPhrase = "well-being hello" ;
		Translator translator = new Translator(inputPhrase) ;
		assertEquals("ellway-eingbay ellohay", translator.translate()) ;
	}
	
	@Test
	public void testTranslationPhraseContainingPunctuations() throws Exception {
		String inputPhrase = "hello world!" ;
		Translator translator = new Translator(inputPhrase) ;
		assertEquals("ellohay orldway!", translator.translate()) ;
	}
	
	

}
