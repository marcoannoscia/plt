package plt.exception;

public class PigLatinException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PigLatinException() {} ;
	
	public PigLatinException(String msg, Throwable cause) {
		super(msg, cause) ;
	}
	
	
}
