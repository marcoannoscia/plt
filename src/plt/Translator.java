package plt;

import plt.exception.PigLatinException;

public class Translator {

	public static final String NIL = "nil" ;
	public static final String SPACE = " " ;
	public static final String DASH = "-" ;
	
	private String phrase ;
	
	
	public Translator(String inputPhrase) {
		phrase = inputPhrase ;
	}

	
	public String getPhrase() {
		return phrase;
	}
	
	public void setPhrase(String phrase){
		this.phrase = phrase ;
	}


	public String translate() throws PigLatinException{
		
		if(phraseContainsPunctuations()) {
			if(!checkPhraseContainsAllowedPunctuations()) {
				throw new PigLatinException("Not Allowed Punctuations", new PigLatinException()) ;
			}else {
				return translatePhraseWithPunctuations(phrase) ;
			}
		}
		
		if(phraseContainsMoreWordsSeparatedBySpaces(phrase) ) {
			return translatePhraseContainingMoreWords(phrase, SPACE) ;
		}
		
		if(phraseContainsMoreWordsSeparatedByDash(phrase) ) {
			return translatePhraseContainingMoreWords(phrase, DASH) ;
		}
		
		if(startWithVowel()){
			if(phrase.endsWith("y")) {
				return phrase + "nay" ;
			}else if(endWithVowel()) {
				return phrase + "yay" ;
			}else{
				return phrase + "ay" ;
			}
		}else if(!phrase.isEmpty()){
			
			if(isVowel(phrase.charAt(1))) {
				return phrase.substring(1) + phrase.charAt(0) + "ay" ;
			}else {
				return appendInitialsConsonantsAndDelete() + "ay" ;
			}

		}
		
		return NIL ;
	
		
	}
	
	
	private boolean startWithVowel() {
		return phrase.startsWith("a") || phrase.startsWith("e") || 
				phrase.startsWith("i") || phrase.startsWith("o") ||
				phrase.startsWith("u") ;
	}
	
	private boolean endWithVowel() {
		return phrase.endsWith("a") || phrase.endsWith("e") || 
				phrase.endsWith("i") || phrase.endsWith("o") ||
				phrase.endsWith("u") ;
	}
	
	
	private boolean isVowel(char c) {
		return c == 'a' || c == 'e' || c == 'i' ||	c == 'o' || c == 'u' ;
	}
	
	
	private String appendInitialsConsonantsAndDelete() {
		String result = phrase ;
		int i = 0 ;
		for( i = 0 ; i < phrase.length() ; i++) {
			if( !isVowel(phrase.charAt(i)) ) {
				result += phrase.charAt(i) ;
			}else {
				break ;
			}
		}
		
		return result.substring(i);
		
	}
	
	private boolean phraseContainsMoreWordsSeparatedBySpaces(String phrase){
		return phrase.contains(SPACE) ;
	}
	
	private boolean phraseContainsMoreWordsSeparatedByDash(String phrase){
		return phrase.contains(DASH) ;
	}
	
	
	
	private String translatePhraseContainingMoreWords(String phrase, String separator) throws PigLatinException {
		String[] phraseSplitted = phrase.split(separator, 2) ;
		String result = "" ;
		Translator translator = new Translator(phraseSplitted[0]) ;
		
		result = translator.translate() ;
		
		if(phraseContainsMoreWordsSeparatedBySpaces(phraseSplitted[1])){
			result += separator ;
			result += translatePhraseContainingMoreWords(phraseSplitted[1], SPACE) ;
		}else if(phraseContainsMoreWordsSeparatedByDash(phraseSplitted[1])){
			result += separator ;
			result += translatePhraseContainingMoreWords(phraseSplitted[1], DASH) ;
		}else{
			translator.setPhrase(phraseSplitted[1]);
			result += separator ;
			result += translator.translate() ;
		}
		
		return result ;
		
	}
	
	
	
	private boolean checkPhraseContainsAllowedPunctuations(){
		
		boolean check = true ;
		
		for (int i = 0 ; i < phrase.length() ; i++) { 
			if(!Character.isLetterOrDigit(phrase.charAt(i)) && phrase.charAt(i) != ' '
					&& phrase.charAt(i) != '-') {
				if(!checkAllowedPunctuation(phrase.charAt(i))) {
					check = false ;
				}
			}
			
		}
		
		return check ;
	}
	
	
	private String translatePhraseWithPunctuations(String phrase) throws PigLatinException {
		String result = "" ;
		String phraseSplitted = "" ;
		Translator translator = new Translator("") ;
		int i = 0 ;
		
		for(i = 0 ; i < phrase.length() ; i++) {
			if(!Character.isLetterOrDigit(phrase.charAt(i)) && phrase.charAt(i)!= ' ' 
					&& phrase.charAt(i)!= '-') {
				translator.setPhrase(phraseSplitted);
				result += translator.translate() ;
				result += phrase.charAt(i);
				phraseSplitted = "" ;
			}else {
				phraseSplitted +=  phrase.charAt(i) ;
			}
		}
		
		if(phraseSplitted != "") {
			translator.setPhrase(phraseSplitted);
			result += translator.translate() ;
		}
		
		return result ;
	}
	
	
	private boolean checkAllowedPunctuation(char toCheck) {

		return toCheck == '.' ||toCheck == ',' || toCheck == ';' ||
				toCheck == ':' ||toCheck == '?' ||toCheck== '!' ||
				toCheck == '\'' || toCheck== '(' || toCheck == ')' ;

	}
	
	
	private boolean phraseContainsPunctuations() {
		for(int i = 0 ; i < phrase.length() ; i++) {
			if(!Character.isLetterOrDigit(phrase.charAt(i)) && phrase.charAt(i)!= ' ' 
					&& phrase.charAt(i)!= '-') {
				return true ;
			}
		}
		return false ;
	}
		
	
}
